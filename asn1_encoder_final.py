#!/usr/bin/env python3
import sys   # do not use any other imports/libraries

def nb(i):
    b = b""
    while i:
        b = bytes([i & 0b11111111]) + b
        i = i >> 8
    return b
    pass

def nb_len(i, length):
    b = b""
    for _ in range(length):
        b = bytes([i & 0b11111111]) + b
        i = i >> 8
    return b
    pass

def nb_7bit_byte(i):
    b = b""
    while i:
        b = bytes([i & 0b01111111 | 0b10000000]) + b
        i = i >> 7
    return b
    pass

def asn1_len(value_bytes):
    length_bytes = nb(len(value_bytes)) or "\x00"

    if len(value_bytes) < 128:
        return length_bytes

    return bytes([0b10000000 | len(length_bytes)]) + length_bytes
    pass

def asn1_boolean(bool):

    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'
    return bytes([0x01]) + asn1_len(bool) + bool
    pass

def asn1_null():
    # returns DER encoding of NULL
    value = 0b00000000
    return bytes([0b00000101]) + asn1_len(value) + value
    pass

def asn1_integer(i):
    b = nb(i) or "\x00"

    # checking if the most significant bit of the most significant (left-most) byte is 1
    if b[0] >> 7:
        b = "\x00" + b

    return bytes([0b00000010]) + asn1_len(b) + b
    pass

def asn1_bitstring(bitstr):
    pad_len = 8 - len(bitstr) % 8

    if pad_len == 8:
        pad_len = 0

    # padding the bitstring
    bitstr+= "0"*pad_len

    # converting bitstring to int
    i = 0
    for bit in bitstr:
        i = i << 1
        if bit=='1':
            i = i | 1

    length_in_bytes = (len(bitstr)+7) / 8
    s = bytes[(pad_len)] + nb_len(i, length_in_bytes)
    return bytes[(0b00000011)] + asn1_len(s) + s
    pass


def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes[(0b00000100)] + asn1_len(octets) + octets 
    pass

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER

    data = bytes([40*oid[0]+oid[1]])
    for i in oid[2:]:
        value = b''
        last_octet = True
        while i:
            if last_octet:
                last_octet = False
                value = bytes([i & 0x7f]) + value
            else:
                value = bytes([i & 0x7f | 0x80]) + value
            i >>= 7
        data += value
    return b'\x06' + asn1_len(data) + data
    pass

def asn1_sequence(der):
    
    return bytes[(0b00110000)] + asn1_len(der) + der 
    pass

def asn1_set(der):

    return bytes[(0b00110001)] + asn1_len(der) + der 
    pass

def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    return bytes[(0b00010011)] + asn1_len(string) + string 
    pass

def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    return bytes[(0b00010111)] + asn1_len(time) + time
    pass

def asn1_tag_explicit(der, tag):
    type_tag = b""
    type_tag = bytes([tag | 0b10100000])
    print(type_tag, type(type_tag))
    return type_tag + asn1_len(der) + der
    pass

# figure out what to put in '...' by looking on ASN.1 structure required (see slides)
#asn1 = asn1_tag_explicit(asn1_sequence(... + asn1_boolean(True) + asn1_bitstring("011") ...), 0)
asn1 = asn1_tag_explicit(asn1_sequence(asn1_set(asn1_integer(5), asn1_integer(200), asn1_integer(65407)) + asn1_boolean(True) + asn1_bitstring("011") + asn1_octetstring(b"abc\x01") + asn1_null() + asn1_objectidentifier([1,2,840,123123]) + asn1_printablestring('hello.') + asn1_utctime(23/0o2/2025 0o1:9:00 GMT)), 0)
open(sys.argv[1], 'wb').write(asn1)

